//Bluethooth package
#include <LBT.h>
#include <LBTClient.h>

//MPU9250 package
#include "Wire.h"
#include "I2Cdev.h"
#include "MPU9250.h"

//定義藍芽連接名稱
#define SPP_SVR "eric"
#define ard_log Serial.printf

//設定九軸變數
MPU9250 accelgyro;
int16_t ax, ay, az;
int16_t gx, gy, gz;
int16_t mx, my, mz;

//設定水位感測變數
const int VAL_PROBE = A0;
 
static LBTDeviceInfo info = {0};

//設定指示燈
int red_led = 5;
int green_led = 6;
int yellow_led = 7;

int read_size;
int a=0;
bool conn_result;
 
 void setup()  
 {
    //九軸初始化
    Wire.begin();
    Serial.begin(9600);
    accelgyro.initialize();
    Serial.println("Initializing I2C devices...");
    Serial.println("Testing device connections...");
    //Serial.println(accelgyro.testConnection() ? "MPU9250 connection successful" : "MPU9250 connection failed");
    ard_log("Bluethooth Start\n");

   //指示燈初始化
   pinMode(red_led, OUTPUT);
   pinMode(green_led, OUTPUT);
   pinMode(yellow_led, OUTPUT);
   digitalWrite(red_led, LOW);
   digitalWrite(green_led, LOW);
   digitalWrite(yellow_led, LOW);
   
   // 藍牙初始化
   bool success = LBTClient.begin();
   if( !success )
   {
       ard_log("Cannot begin Bluetooth Client successfully\n");
       delay(0xffffffff);
   }
   else
   {
       ard_log("Bluetooth Client begin successfully\n");
       // 收尋周邊的藍芽裝置
       int num = LBTClient.scan(30);
       ard_log("scanned device number [%d]", num);
       for (int i = 0; i < num; i++)
       {
         memset(&info, 0, sizeof(info));
         // to check the prefer master(server)'s name
         if (!LBTClient.getDeviceInfo(i, &info))
         {
             continue;
         }
         ard_log("getDeviceInfo [%02x:%02x:%02x:%02x:%02x:%02x][%s]", 
             info.address.nap[1], info.address.nap[0], info.address.uap, info.address.lap[2], info.address.lap[1], info.address.lap[0],
             info.name);
         if (0 == strcmp(info.name, SPP_SVR))
         {
             ard_log("found\n");
             break;
         }
       }
       
   }
 }
  
 void loop()
 {
    //讀取九軸資料
    accelgyro.getMotion9(&ax, &ay, &az, &gx, &gy, &gz, &mx, &my, &mz);
    Serial.print("a/g/m:\t");
    Serial.print(ax); Serial.print("\t");
    Serial.print(ay); Serial.print("\t");
    Serial.print(az); Serial.print("\t");
    Serial.print(gx); Serial.print("\t");
    Serial.print(gy); Serial.print("\t");
    Serial.print(gz); Serial.print("\t");
    Serial.print(mx); Serial.print("\t");
    Serial.print(my); Serial.print("\t");
    Serial.println(mz);
    
    //讀取水位資料
    int moisture = analogRead(VAL_PROBE);
    Serial.print(" Moisture level:\t");
    Serial.println(moisture);

    //藍芽連結
    if(a==0){
    conn_result = LBTClient.connect(info.address);
    }

    //是否連結     
    if( !conn_result ){
    ard_log("Cannot connect to SPP Server successfully\n");
    delay(0xffffffff);
    a=0;
    }else{
    ard_log("Connect to SPP Server successfully\n");
    a=1;
    }

    //淹水
    if(moisture>400){
      int write_size = LBTClient.write((uint8_t*)"0", 12);
      ard_log("client first spec write_size [%d]", write_size);
      digitalWrite(yellow_led, HIGH);
    }else{
      digitalWrite(yellow_led, LOW);
    }

    //有停車
    if(mz>1000){
      int write_size = LBTClient.write((uint8_t*)"1", 12);
      ard_log("client first spec write_size [%d]", write_size);
      digitalWrite(red_led, HIGH);
    }else{
      digitalWrite(red_led, LOW);
    }
    
    //沒有停車
    if(mz<-100){
      int write_size = LBTClient.write((uint8_t*)"2", 12);
      ard_log("client first spec write_size [%d]", write_size);
      digitalWrite(green_led, HIGH);
    }else{
      digitalWrite(green_led, LOW);
    }
        
     delay(2000);
 
 }
