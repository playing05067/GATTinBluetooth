#include "DHT.h"
#include "Temperature_Service.h"
#include <LGATTUUID.h>
#include <LBTServer.h>
 
#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);
 
float temperature = 0.0;
 
#ifdef APP_LOG
#undef APP_LOG
#endif
 
#define APP_LOG(...) Serial.printf(__VA_ARGS__); \
    Serial.println();
 
Temperature_Service temperature_service;
 
void setup() {
  
  Serial.begin(115200);
  dht.begin();
   
  delay(8000);
  //LBTServer.begin((uint8_t*)"1234");
  //LBTServer.end();

  //決定你要啟動幾個serve
  if (!LGATTServer.begin(1, &temperature_service)) {
      APP_LOG("GATT Server failed");
    }
  else{
      APP_LOG("GATT Server started!");
  }
}
 
void loop() {
  delay(5000);
  LGATTServer.handleEvents();
  if (temperature_service.isConnected())
  {
    float h = 0.0;
    float t = 0.0;
    if (dht.readHT(&t, &h))
    {
      if(t!=temperature){
          temperature = t;
 
          //創一個新的LGATTAttributeValue
          LGATTAttributeValue value = {0};
          //創一個temperature_string 字串
          String temperature_string = "Temperature "+String(temperature);
          const char *str = temperature_string.c_str();
          value.len = strlen(str);
          //拷貝值到 LGATTAttributeValue
          memcpy(value.value, str, value.len);
          // 送一個指令 GATT client
          boolean ret = temperature_service.sendIndication(value, temperature_service.getHandleNotify(), false);
          // 將訊息顯示在監控視窗中，顯示送的指令失敗還是成功
          if (!ret)
          {
              APP_LOG("[FAILED] send [%d]", ret);
          }
              APP_LOG("send [%d][%s]", ret, value.value);
      }
      
    }
  }
}
