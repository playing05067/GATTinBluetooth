#include "DHT.h"
#include "Temperature_Service.h"
#include <LGATTUUID.h>
 
 
#define DHTPIN 0     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);
 
float temperature = 0.0;
 
#ifdef APP_LOG
#undef APP_LOG
#endif
 
#define APP_LOG(...) Serial.printf(__VA_ARGS__); \
    Serial.println();
 
Temperature_Service temperature_service;
 
void setup() {
   
  Serial.begin(115200);
  dht.begin();
   
  delay(8000);
 
  if (!LGATTServer.begin(1, &temperature_service)) {
      APP_LOG("GATT Server failed");
    }
  else{
      APP_LOG("GATT Server started!");
  }
}
 
void loop() {
  // put your main code here, to run repeatedly:
  delay(5000);
  LGATTServer.handleEvents();
  //LGATTAttributeValue value = {13, "hello, world!"};
  if (temperature_service.isConnected())
  {
    float h = 0.0;
    float t = 0.0;
    if (dht.readHT(&t, &h))
    {
      if(t!=temperature){
          temperature = t;
 
          //Create a new LGATTAttributeValue
          LGATTAttributeValue value = {0};
          //Build a string with the temperature readout
          String temperature_string = "Temperature "+String(temperature);
          const char *str = temperature_string.c_str();
          value.len = strlen(str);
          //copy the string into the LGATTAttributeValue
          memcpy(value.value, str, value.len);
          //Send an indication to the GATT client
          boolean ret = temperature_service.sendIndication(value, temperature_service.getHandleNotify(), false);
 
          if (!ret)
          {
              APP_LOG("[FAILED] send [%d]", ret);
          }
          APP_LOG("send [%d][%s]", ret, value.value);
      }
    }
  }
}
